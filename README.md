# Copyright year formatter addon for statamic

## Installation
Copy addon's source code to project's `site/addons` directory.

## Usage
Display formatted copyright year in page footer:
```
 <p>&copy; {{ footer_content:copyright_year | copyright_year }}. All rights reserved.</p>
```

If copyright year is current year, this year is basically displayed. If copyright year is in the past, year range is displayed.

### Sample output 1
Current year is 2020. Copyright year is set to 2020. Output: `2020`

### Sample output 2
Current year is 2020. Copyright year is set to 2019. Output: `2019-2020`