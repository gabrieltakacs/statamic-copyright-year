<?php

namespace Statamic\Addons\CopyrightYear;

use Statamic\Extend\Modifier;

class CopyrightYearModifier extends Modifier
{
    /**
     * Maps to {{ var | copyright_year }} or {{ var copyright_year="..." }}
     *
     * @param mixed  $value    The value to be modified
     * @param array  $params   Any parameters used in the modifier
     * @param array  $context  Contextual values
     * @return mixed
     */
    public function index($value, $params, $context)
    {
        $currentYear = date('Y');

        if ($value >= $currentYear) {
            return $value;
        }

        return $value . ' - ' . $currentYear;
    }
}
